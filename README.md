# Open/close Principle

Se ha construido un package 'primos' con un metodo GeneratePrimos que devuelve
los numeros primos entre 2 y el numero indicado.

Modifica la clase considerando una nueva funcionalidad pero ¿que nuevo feature tenemos previsto?

Se desea la creación de un nuevo tipo de Dato que permita la incrustación del tipo de Dato 'Primos' y que permita realizar el
orden inverso de los datos ya creados permitiendo asi superponer los metodos del tipo de dato 'Primos'

Se tiene un test que realiza dos pruebas: TestGeneratePrimos_ordenNatural y TestGeneratePrimos_ordenInverso

La clase original (GeneradorPrimos) supera el TestGeneratePrimos_ordenNatural. 

Debes realizar tres pasos:<br />
1 - Modifica el package 'primos' agregando un nuevo tipo de dato que permita incrustar el tipo de dato 'Primos'. <br />
2 - Crea un nuevo metodo que superponga el metodo GeneratePrimos y permita devolverlos de manera inversa. <br />
3 - Adapta el test para que TestGeneratePrimos_ordenInverso use este nuevo metodo. <br />
4.- Crees que se puede optimizar el codigo fuente desarrollado como? en que funcion/metodo?<br />