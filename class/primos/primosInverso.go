// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          primos.go                             */
/* --                                                                */
/* -- Descripcion: Clase que mostrara los numeros primos de manera   */
/* --              inversa                                           */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/
package primos

import "sort"

func (p XPrimos) GeneratePrimos() []int32 {
	var originalP Primos
	var slicePrimosInt []int
	originalP.N = p.N
	slicePrimos := originalP.GeneratePrimos()
	for _, value := range slicePrimos {
		slicePrimosInt = append(slicePrimosInt, int(value))
	}
	sort.Sort(sort.Reverse(sort.IntSlice(slicePrimosInt)))
	for key, val := range slicePrimosInt {
		slicePrimos[key] = int32(val)
	}
	return slicePrimos
}
